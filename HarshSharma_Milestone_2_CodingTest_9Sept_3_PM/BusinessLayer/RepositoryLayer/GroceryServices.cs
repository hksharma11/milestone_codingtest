﻿using HarshSharma_Milestone_2_CodingTest_9Sept_3_PM.BusinessLayer.Persistence;
using HarshSharma_Milestone_2_CodingTest_9Sept_3_PM.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HarshSharma_Milestone_2_CodingTest_9Sept_3_PM.BusinessLayer.RepositoryLayer
{
    public class GroceryServices : IGroceryServices
    {

        private GroceryDBContext db;

        public GroceryServices(GroceryDBContext db)
        {
            this.db = db;
        }

        public IEnumerable<Product> GetAllProducts()
        {
            return db.Product.ToList();
        }

        public IEnumerable<Categories> GetCategories()
        {
            return db.Categories.ToList();
        }

        public IEnumerable<ProductOrder> GetOrders(int userId)
        {
            return db.ProductOrder.Where(x => x.Userid == userId);
        }

        public Product GetProductById(int productId)
        {
            return db.Product.SingleOrDefault(x => x.Productid == productId);
        }

        public ProductOrder PlaceOrder(int userId, int productId)
        {
            var order = new ProductOrder()
            {

                Productid = productId,
                Userid = userId,
                Orderid = db.ProductOrder.Max(x => x.Orderid) + 1,
                User = db.ApplicationUser.SingleOrDefault(x => x.Id == userId),
                Product = db.Product.SingleOrDefault(x => x.Productid == productId)

            };
            db.ProductOrder.Add(order);
            db.SaveChanges();
            return order;
        }
    }
}
