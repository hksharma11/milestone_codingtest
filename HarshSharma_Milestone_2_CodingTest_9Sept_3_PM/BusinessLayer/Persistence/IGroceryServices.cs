﻿using HarshSharma_Milestone_2_CodingTest_9Sept_3_PM.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HarshSharma_Milestone_2_CodingTest_9Sept_3_PM.BusinessLayer.Persistence
{
    public interface IGroceryServices
    {
        IEnumerable<Product> GetAllProducts();

        Product GetProductById(int productId);

        IEnumerable<Categories> GetCategories();

        IEnumerable<ProductOrder> GetOrders(int userId);

        ProductOrder PlaceOrder(int userId, int productId);
    }
}
