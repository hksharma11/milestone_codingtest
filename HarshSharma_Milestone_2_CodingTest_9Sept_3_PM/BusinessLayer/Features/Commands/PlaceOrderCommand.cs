﻿using HarshSharma_Milestone_2_CodingTest_9Sept_3_PM.Entities;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HarshSharma_Milestone_2_CodingTest_9Sept_3_PM.BusinessLayer.Features.Commands
{
    public class PlaceOrderCommand : IRequest<ProductOrder>
    {
        public int UserId { get; set; }

        public int ProductId { get; set; }
    }
}
