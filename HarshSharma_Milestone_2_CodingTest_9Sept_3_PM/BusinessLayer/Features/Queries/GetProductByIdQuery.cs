﻿using HarshSharma_Milestone_2_CodingTest_9Sept_3_PM.Entities;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HarshSharma_Milestone_2_CodingTest_9Sept_3_PM.BusinessLayer.Features.Queries
{
    public class GetProductByIdQuery : IRequest<Product>
    {
        public int productId { get; set; }
    }
}
