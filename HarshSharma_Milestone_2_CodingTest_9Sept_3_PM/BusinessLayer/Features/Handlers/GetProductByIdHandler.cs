﻿using HarshSharma_Milestone_2_CodingTest_9Sept_3_PM.BusinessLayer.Features.Queries;
using HarshSharma_Milestone_2_CodingTest_9Sept_3_PM.BusinessLayer.Persistence;
using HarshSharma_Milestone_2_CodingTest_9Sept_3_PM.Entities;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace HarshSharma_Milestone_2_CodingTest_9Sept_3_PM.BusinessLayer.Features.Handlers
{
    public class GetProductByIdHandler : IRequestHandler<GetProductByIdQuery, Product>
    {
        private IGroceryServices groceryService;

        public GetProductByIdHandler(IGroceryServices groceryService)
        {
            this.groceryService = groceryService;
        }

        public async Task<Product> Handle(GetProductByIdQuery request, CancellationToken cancellationToken)
        {
            return await Task.FromResult(groceryService.GetProductById(request.productId));
        }
    }
}
