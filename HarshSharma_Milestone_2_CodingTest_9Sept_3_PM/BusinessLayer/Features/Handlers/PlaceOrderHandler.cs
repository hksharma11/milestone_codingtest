﻿using HarshSharma_Milestone_2_CodingTest_9Sept_3_PM.BusinessLayer.Features.Commands;
using HarshSharma_Milestone_2_CodingTest_9Sept_3_PM.BusinessLayer.Persistence;
using HarshSharma_Milestone_2_CodingTest_9Sept_3_PM.Entities;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace HarshSharma_Milestone_2_CodingTest_9Sept_3_PM.BusinessLayer.Features.Handlers
{
    public class PlaceOrderHandler : IRequestHandler<PlaceOrderCommand, ProductOrder>
    {

        private IGroceryServices groceryService;

        public PlaceOrderHandler(IGroceryServices groceryService)
        {
            this.groceryService = groceryService;
        }

        public async Task<ProductOrder> Handle(PlaceOrderCommand request, CancellationToken cancellationToken)
        {
            return await Task.FromResult(groceryService.PlaceOrder(request.UserId, request.ProductId));
        }
    }
}
