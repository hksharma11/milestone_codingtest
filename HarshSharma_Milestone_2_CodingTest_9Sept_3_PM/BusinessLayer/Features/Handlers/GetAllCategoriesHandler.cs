﻿using HarshSharma_Milestone_2_CodingTest_9Sept_3_PM.BusinessLayer.Features.Queries;
using HarshSharma_Milestone_2_CodingTest_9Sept_3_PM.BusinessLayer.Persistence;
using HarshSharma_Milestone_2_CodingTest_9Sept_3_PM.Entities;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace HarshSharma_Milestone_2_CodingTest_9Sept_3_PM.BusinessLayer.Features.Handlers
{
    public class GetAllCategoriesHandler : IRequestHandler<GetAllCategoriesQuery, IEnumerable<Categories>>
    {

        private IGroceryServices groceryServices;

        public GetAllCategoriesHandler(IGroceryServices groceryServices)
        {
            this.groceryServices = groceryServices;
        }

        public async Task<IEnumerable<Categories>> Handle(GetAllCategoriesQuery request, CancellationToken cancellationToken)
        {
            return await Task.FromResult(groceryServices.GetCategories());
        }
    }
}
