﻿using System;
using System.Collections.Generic;

namespace HarshSharma_Milestone_2_CodingTest_9Sept_3_PM.Entities
{
    public partial class Categories
    {
        public Categories()
        {
            Product = new HashSet<Product>();
        }

        public int Catid { get; set; }
        public string Catname { get; set; }

        public virtual ICollection<Product> Product { get; set; }
    }
}
