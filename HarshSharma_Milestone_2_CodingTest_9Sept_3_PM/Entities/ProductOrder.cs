﻿using System;
using System.Collections.Generic;

namespace HarshSharma_Milestone_2_CodingTest_9Sept_3_PM.Entities
{
    public partial class ProductOrder
    {
        public int Orderid { get; set; }
        public int Productid { get; set; }
        public int Userid { get; set; }

        public virtual Product Product { get; set; }
        public virtual ApplicationUser User { get; set; }
    }
}
