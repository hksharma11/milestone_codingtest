﻿using System;
using System.Collections.Generic;

namespace HarshSharma_Milestone_2_CodingTest_9Sept_3_PM.Entities
{
    public partial class MenuBar
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string Url { get; set; }
        public bool? Openinnewwindow { get; set; }
    }
}
