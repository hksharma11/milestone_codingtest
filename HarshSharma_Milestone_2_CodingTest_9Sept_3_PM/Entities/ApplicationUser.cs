﻿using System;
using System.Collections.Generic;

namespace HarshSharma_Milestone_2_CodingTest_9Sept_3_PM.Entities
{
    public partial class ApplicationUser
    {
        public ApplicationUser()
        {
            ProductOrder = new HashSet<ProductOrder>();
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public long? Phone { get; set; }
        public int? Pincode { get; set; }
        public string House { get; set; }
        public string Road { get; set; }
        public string City { get; set; }
        public string State { get; set; }

        public virtual ICollection<ProductOrder> ProductOrder { get; set; }
    }
}
