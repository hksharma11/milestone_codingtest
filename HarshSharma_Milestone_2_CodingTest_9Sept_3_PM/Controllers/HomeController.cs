﻿using HarshSharma_Milestone_2_CodingTest_9Sept_3_PM.BusinessLayer.Features.Commands;
using HarshSharma_Milestone_2_CodingTest_9Sept_3_PM.BusinessLayer.Features.Queries;
using HarshSharma_Milestone_2_CodingTest_9Sept_3_PM.Entities;
using MediatR;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HarshSharma_Milestone_2_CodingTest_9Sept_3_PM.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class HomeController : ControllerBase
    {

        private IMediator mediator;

        public HomeController(IMediator mediator)
        {
            this.mediator = mediator;
        }

        [HttpGet]
        public async Task<IEnumerable<Product>> GetAllProducts()
        {
            return await mediator.Send(new GetAllProductsQuery());
        }

        [HttpGet]
        public async Task<IEnumerable<Categories>> GetAllCategories()
        {
            return await mediator.Send(new GetAllCategoriesQuery());
        }

        [HttpGet]
        public async Task<IEnumerable<ProductOrder>> GetOrderInfo(int userId)
        {
            return await mediator.Send(new GetOrderInfoQuery { userid = userId });
        }

        [HttpGet]
        public async Task<Product> GetProductById(int productId)
        {
            return await mediator.Send(new GetProductByIdQuery { productId = productId });
        }

        [HttpPost]
        public async Task<ProductOrder> PlaceOrder(int userId, int productId)
        {
            return await mediator.Send(new PlaceOrderCommand { UserId = userId, ProductId = productId });
        }
    }
}
